export default interface IPost {
  email: string
  name?: string
  password: string
  group?: string
}
