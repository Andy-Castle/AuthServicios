import { ref } from 'vue'
import type { Ref } from 'vue'
import type IPost from '@/interfaces/IPost'

const url = import.meta.env.VITE_API_URL || 'http://192.168.80.116:3000'

export default class PostService {
  private posts: Ref<IPost[]>
  private post: Ref<IPost>

  constructor() {
    this.posts = ref([])
    this.post = ref({} as IPost)
  }

  getPosts(): Ref<IPost[]> {
    return this.posts
  }

  getPost(): Ref<IPost> {
    return this.post
  }

  async fetchAll(): Promise<void> {
    try {
      const response = await fetch(url + '/posts')
      const data = await response.json()
      this.posts.value = data
    } catch (error) {
      console.error(error)
    }
  }

  async fetchPost(id: string): Promise<void> {
    try {
      const response = await fetch(url + '/posts/' + id)
      const data = await response.json()
      this.post.value = data
    } catch (error) {
      console.error(error)
    }
  }

  addUser = async (user: Ref<IPost>): Promise<void> => {
    try {
      const response = await fetch(url + '/register', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(user.value)
      })
      const data = await response.json()
      console.log('Usuario añadido:', data)
    } catch (error) {
      console.error(error)
    }
  }

  addLogin = async (user: Ref<IPost>): Promise<void> => {
    try {
      const response: any = await fetch(url + '/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(user.value)
      })
      const data = await response.json()
      localStorage.setItem('TOKENID', data.token)
      console.log('Usuario logeado:', data)
    } catch (error) {
      console.error(error)
    }
  }
}
